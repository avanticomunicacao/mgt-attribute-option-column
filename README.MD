# Módulo para Magento 2 Avanti AttributeOptionColumn

    ``penseavanti/module-attributeoptioncolumn``

## Principais funcionalidades

Este módulo tem a função de adicionar uma coluna nas opções de atributo para exibir o ID da opção.
Foi desenvolvido especialmente para o projeto Madeiranit.

## Instalação

### Modo 1: Arquivo ZIP

 - Descompactar o arquivo ZIP na pasta `app/code/Avanti`
 - Habilitar o módulo executando no terminal `php bin/magento module:enable Avanti_AttributeOptionColumn`
 - Aplique atualizações do banco de dados executando `php bin/magento setup:upgrade`
 - Limpe o cache `php bin/magento cache:flush`

### Modo 2: Composer

 - Inclua o repositório do Composer na configuração executando `composer config repositories.mgt-attribute-option-column git git@bitbucket.org:avanticomunicacao/mgt-attribute-option-column.git`
 - Instale o módulo via Composer `composer require penseavanti/module-attributeoptioncolumn`
 - Habilitar o módulo executando no terminal `php bin/magento module:enable Avanti_AttributeOptionColumn`
 - Aplique atualizações do banco de dados executando `php bin/magento setup:upgrade`\*
 - Limpe o cache `php bin/magento cache:flush`

## Configuração

Dentro do seu tema você crie o arquivo {{DIRETORIO_DO_TEMA_ADMIN}}/Magento_Catalog/layout/catalog_product_attribute_edit.xml

Insira no arquivo:

```xml
<?xml version="1.0"?>
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="admin-2columns-left" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceBlock name="main.advanced">
            <action method="setTemplate">
                <argument name="template" xsi:type="string">Avanti_AttributeOptionColumn::catalog/product/attribute/options.phtml</argument>
            </action>
        </referenceBlock>
    </body>
</page>
```

Dentro do seu tema você crie o arquivo {{DIRETORIO_DO_TEMA_ADMIN}}/Magento_Swatches/layout/catalog_product_attribute_edit.xml

Insira no arquivo:

```xml
<?xml version="1.0"?>
<page xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" layout="admin-2columns-left" xsi:noNamespaceSchemaLocation="urn:magento:framework:View/Layout/etc/page_configuration.xsd">
    <body>
        <referenceBlock name="main.swatches_visual">
            <action method="setTemplate">
                <argument name="template" xsi:type="string">Avanti_AttributeOptionColumn::catalog/product/attribute/visual.phtml</argument>
            </action>
        </referenceBlock>
    </body>
</page>
```
